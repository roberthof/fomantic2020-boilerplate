class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  extend FriendlyId
	friendly_id :uuid, use: [:slugged, :finders]

  private

	  def uuid
	    slug || SecureRandom.uuid
	  end

end
