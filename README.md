# README

Standard Fomantic Rails build for 2020. Includes:

* Ruby 2.7.1

* Rails 6.0.2.1

* Fomantic-UI 2.8 with almost everything in app/javascripts (Sass version)
